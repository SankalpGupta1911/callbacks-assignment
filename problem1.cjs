const fs = require("fs");
const path = require("path");

function createDirectory(dirName, callback) {
  const dirPath = path.join(__dirname, dirName);

  fs.mkdir(dirPath, (err) => callback(err));
}

function deleteAllFiles(dirName, callback) {
  fs.readdir(path.join(__dirname, dirName), (err, files) => {
    if (err) {
      callback(err);
    } else {
      console.log(
        `Inside : ${path.join(__dirname, dirName)}. Now deleting files...`
      );
      files.forEach((file) => {
        fs.unlink(path.join(__dirname, dirName, file), (err) => {
          if (err) {
            callback(err);
          } else {
            console.log(`The file: ${file} has been deleted successfully!`);
          }
        });
      });
    }
  });
}

function createAllFiles(dirName, numberOfFiles, callback) {
  let completedExecution = 0;
  for (let idx = 1; idx <= Number(numberOfFiles); idx++) {
    let nameIndex = Math.floor(Math.random() * 1000).toString();
    let fileName = nameIndex.concat(".json");
    let dirPath = path.join(__dirname, dirName, fileName);
    fs.writeFile(dirPath, "Assume Content!", (err) => {
      if (err) {
        callback(err);
      } else {
        completedExecution+=1;
        console.log(`The file: ${fileName} has been created!`);
      }
      if ( completedExecution === Number(numberOfFiles)) {
        console.log(`All ${numberOfFiles} files have been created!`)
        callback()};
    });
  }
}

function Problem1(
  dirName = "Random Directory ".concat((Math.floor(Math.random()*1000)).toString()),
  numberOfFiles = Math.floor(Math.random() * 100)
) {
  createDirectory(dirName, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log(`The directory ${dirName} has been created!`);
      createAllFiles(dirName, numberOfFiles, (err) => {
        if (err) {
          console.error(err);
        } else {
          deleteAllFiles(dirName, (err) => {
            if (err) {
              console.error(err);
            } else {
              console.log("Task has been completed!");
            }
          });
        }
      });
    }
  });
}

module.exports = Problem1;
