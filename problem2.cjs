const fs = require('fs');
const path = require('path');

let filePath = path.join(__dirname, 'lipsum.txt')


function readFile(fileName, callback){

    // filePath=fileName)
    fs.readFile(fileName, 'utf-8' ,(err, fileData) => {
        if (err){
            callback(err);
        } else {
            callback(null, fileData)
        } 
    });
}


function convertToUpperCase(data, callback){
    setTimeout(() => {
        try {
          const capsData = data.toUpperCase();
          callback(null, capsData);
        } catch (err) {
          callback(err);
        }
      }, 0);
}

function getOnlyFileName(fileName, callback){

    setTimeout(()=>{
        try {
            const newName = fileName.split('.')[0];
            callback(null, newName);
        } catch(err) {
            callback(err);
        }
    }, 0);

}

function createRandomTextFileName(formerName, callback){

    setTimeout(()=>{
        try {
        const newName = formerName.concat(Math.floor(Math.random()*20)).concat('.txt');
        callback(null, newName);
    } catch(err){
        callback(err);
    }}, 0)
}


function convertToLowerCase(data, callback){
    setTimeout(() => {
        try {
          const lowerData = data.toLowerCase();
          callback(null, lowerData);
        } catch (err) {
          callback(err);
        }
      }, 0);
}

function writeFile(fileName, data, callback){
    
    fs.writeFile(fileName , data.concat('\n'), (err)=> {
        if(err){
            callback(err);
        } else{
            callback(null);
        }
    });
}

function splitContents(data, splitter, callback){

    setTimeout(()=>{
        try{
            const splitData = data.split(splitter);
            callback(null, splitData);
        }
        catch(err){
            callback(err);
        }
    },0)
}

function appendDataToFile(fileName, data, callback){
    if(typeof data==='string'|| typeof data ==='number'){
        setTimeout(()=>{
            try{
                const newData = data.concat('\n')
                fs.appendFile(fileName, newData, (err)=>{
                    if(err){
                        callback(err);
                    } 
                })
                callback(null);
            }
            catch(err){
                callback(err);
            }
        },0)
    }
    else if(typeof data==='object'){
        setTimeout(()=>{
            try{
                let counter = 0;
                for(let idx = 0; idx<data.length; idx++){
                    let newLine = data[idx].concat('\n');
                    fs.appendFile(fileName, newLine,  (err)=>{
                        if(err){
                            callback(err);
                        } else {
                            counter++;
                        }
                    })
                    if(counter === data){
                        callback(null);
                    }
                }
                callback(null);
            }
            catch(err){
                callback(err);
            }
        },0)

    }
}

function sortContent(data, callback){
    setTimeout(()=>{
        try{
            let sortedData = data.sort();
            callback(null,sortedData);
        } catch(err) {
            callback(err);
        }
    },0)
}

function deleteFilesFromList(fileList, callback){
    let counter = 0;
    for(let idx = 0; idx<fileList.length; idx++){
        let currFile = fileList[idx];
        deleteAFile(currFile, (err)=>{
            if(err){
                callback(err);
            } else {
                counter++;
            }
        if( counter === fileList.length ){
            callback()
        }
        })
    }
}

function deleteAFile(file, callback){
    fs.unlink(file, (err)=>{
        if(err){
            callback(err)
        }else{
            callback()
        }
    })
}

function problem2(fileName='lipsum.txt'){

    readFile(fileName, (err, data)=>{
        if (err){
            console.error(err);
        } else {
            console.log('Read the lipsum.txt file');
            convertToUpperCase(data, (err, upperCaseData)=>{
                if(err){
                    console.error(err);
                } else {
                    console.log('Converted the content to upper case');
                    getOnlyFileName(fileName, (err, newFileName)=>{
                        if(err){
                            console.error(err);
                        } else {
                            console.log(`The file name is ${newFileName}.`);
                            createRandomTextFileName(newFileName, (err, randomFileName)=>{
                                if(err){
                                    console.error(err);
                                } else {
                                    console.log(`The random file name is : ${randomFileName}`);
                                    writeFile(randomFileName, upperCaseData, (err)=>{
                                        if(err){
                                            console.error(err);
                                        } 
                                        else { 
                                            console.log(`The ${randomFileName} file has been created with the upper case data.`);
                                            writeFile('filenames.txt', randomFileName, (err)=>{
                                                if(err){
                                                    console.error(err);
                                                } else {
                                                    console.log(`The new file name has been written in the file: filenames.txt \n`);
                                                    readFile(randomFileName, (err, data)=>{
                                                        if (err){
                                                            console.error(err);
                                                        } else {
                                                            console.log(`Read the ${randomFileName} file.`);
                                                            convertToLowerCase(data, (err, lowerCaseData)=>{
                                                                if(err){
                                                                    console.error(err);
                                                                } else {
                                                                    console.log(`Converted the data to Lower case`);
                                                                    writeFile(randomFileName, lowerCaseData, (err)=>{
                                                                        if(err){
                                                                            console.error(err);
                                                                        } else {
                                                                            console.log(`Wrote the lower case data to ${randomFileName}`);
                                                                            splitContents(lowerCaseData, '. ',(err, splitData)=>{
                                                                                if(err){
                                                                                    console.error(err);
                                                                                } else {
                                                                                    console.log(`Split the contents into sentences`);
                                                                                    getOnlyFileName(randomFileName, (err, newFileName)=>{
                                                                                        if(err){
                                                                                            console.error(err);
                                                                                        } else {
                                                                                            console.log(`The file name is ${newFileName}.`);
                                                                                            createRandomTextFileName(newFileName, (err, randomFileName)=>{
                                                                                                if(err){
                                                                                                    console.error(err);
                                                                                                } else {
                                                                                                    writeFile(randomFileName, '', (err)=>{
                                                                                                        if(err){
                                                                                                            console.error(err);
                                                                                                        } else{
                                                                                                            console.log(`New file created with name ${randomFileName}.`);
                                                                                                            appendDataToFile(randomFileName, splitData, (err)=>{
                                                                                                                if(err){
                                                                                                                    console.error(err);
                                                                                                                }
                                                                                                                else {
                                                                                                                    console.log(`the data has been appended to ${randomFileName}`);
                                                                                                                    appendDataToFile('filenames.txt', randomFileName, (err)=>{
                                                                                                                        if(err){
                                                                                                                            console.error(err);
                                                                                                                        } else {
                                                                                                                            console.log(`The name of the ${randomFileName} has been added to filenames.txt`);
                                                                                                                            readFile(randomFileName, (err, data)=>{
                                                                                                                                if(err){
                                                                                                                                    console.error(err);
                                                                                                                                } else {
                                                                                                                                    splitContents(data, '\n', (err, splitData)=>{
                                                                                                                                        if(err){
                                                                                                                                            console.error(err);
                                                                                                                                        } else {
                                                                                                                                            sortContent(splitData, (err,sortedData)=>{
                                                                                                                                                if(err){
                                                                                                                                                    console.error(err);
                                                                                                                                                } else {
                                                                                                                                                    console.log(`The Data has been sorted.`);
                                                                                                                                                    getOnlyFileName(randomFileName, (err, newFileName)=>{
                                                                                                                                                        if(err){
                                                                                                                                                            console.error(err);
                                                                                                                                                        } else {
                                                                                                                                                            createRandomTextFileName(newFileName, (err, randomFileName2)=>{
                                                                                                                                                                if(err){
                                                                                                                                                                    console.error(err);
                                                                                                                                                                } else {
                                                                                                                                                                    writeFile(randomFileName2, '', (err)=>{
                                                                                                                                                                        if(err){
                                                                                                                                                                            console.error(err);
                                                                                                                                                                        } else {
                                                                                                                                                                            appendDataToFile(randomFileName2, sortedData, (err)=>{
                                                                                                                                                                                if(err){
                                                                                                                                                                                    console.error(err);
                                                                                                                                                                                } else{
                                                                                                                                                                                    console.log(`The file ${randomFileName2} has been created`);
                                                                                                                                                                                    appendDataToFile(appendDataToFile('filenames.txt', randomFileName2, (err)=>{
                                                                                                                                                                                        if(err){
                                                                                                                                                                                            console.error(err);
                                                                                                                                                                                        } else {
                                                                                                                                                                                            console.log(`The file name: ${randomFileName2} has been added to filenames.txt`);
                                                                                                                                                                                            readFile('filenames.txt', (err, fileListAsText)=>{
                                                                                                                                                                                                if(err){
                                                                                                                                                                                                    console.error(err);
                                                                                                                                                                                                } else {
                                                                                                                                                                                                    splitContents(fileListAsText, '\n', (err, fileList)=>{
                                                                                                                                                                                                        if(err){
                                                                                                                                                                                                            console.error(err);
                                                                                                                                                                                                        } else {
                                                                                                                                                                                                            fileList = fileList.filter(element => element);
                                                                                                                                                                                                            console.log(fileList);
                                                                                                                                                                                                            deleteFilesFromList(fileList, (err)=>{
                                                                                                                                                                                                                if(err){
                                                                                                                                                                                                                    console.error(err);
                                                                                                                                                                                                                } else {
                                                                                                                                                                                                                    console.log('Deleted all the files in filenames.txt')
                                                                                                                                                                                                                }
                                                                                                                                                                                                            })
                                                                                                                                                                                                        }
                                                                                                                                                                                                    })
                                                                                                                                                                                                }
                                                                                                                                                                                            })
                                                                                                                                                                                        }
                                                                                                                                                                                    }))
                                                                                                                                                                                }
                                                                                                                                                                            })
                                                                                                                                                                        }
                                                                                                                                                                    })
                                                                                                                                                                }
                                                                                                                                                            })
                                                                                                                                                        }
                                                                                                                                                    })
                                                                                                                                                }
                                                                                                                                            })
                                                                                                                                        }
                                                                                                                                    })
                                                                                                                                }
                                                                                                                            })
                                                                                                                        }
                                                                                                                    })
                                                                                                                }
                                                                                                            })
                                                                                                        }
                                                                                                    } )
                                                                                                }
                                                                                            })  
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    });
                }
            });
        }
    })
}

module.exports = problem2;